# ColorSwap

A utility to swap one hue with another (within a threshold) in an image or folder of images.

## Requirements

ColorSwap requires Python 3 and the Pillow library. Install Pillow as follows:

```sh
pip install -U pillow
```

ColorSwap has been tested to run correctly on Python 3.11.1 and Pillow 9.4.0.

## Usage

Run `python ./color_swap.py -h` for the help message.

The default threshold is 20 degrees.

The default filter matches all `.tga` and `.tga.dds` files.

ColorSwap can read DXT1 and DXT5 compressed images. It cannot read LZ4 compressed images. Outputs are uncompressed.

_NOTE: ColorSwap expects hue values in degrees from 0 to 360, not RGB values. Most image editing software color pickers will display both RGB and HSV values._

### Example

```sh
python ./color_swap.py ./input/ ./output/ 140 270 -t 15 -f *_armenia*
```

Running the above command will find all files within the `input` folder whose filenames contain `_armenia`, swap all hues within 15 degrees of 140 (green) with corresponding hues around 270 degrees (purple), and save the color-swapped images into the `output` folder, preserving the folder structure.

If our folder structure was as follows,

```
/input
    /armenia
        /east_general_armenia.tga.dds
        /east_hillmen_armenia_c.tga.dds
    /parthia
        /east_peasant_parthia.tga.dds
        east_general_parthia.tga.dds
/output
    /armenia
```

then our command would only process the two textures containing `_armenia` in their filenames.

_NOTE: ColorSwap does not currently recreate the input folder structure. Therefore, you must recreate the structure (`/output/armenia` in the above example) before running your command._

Brought to you by the EB Online Team
