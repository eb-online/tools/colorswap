import argparse
import os
import time
from concurrent.futures import ProcessPoolExecutor
from itertools import repeat
from pathlib import Path

from PIL import Image

VERSION = "0.1"

ERR_MISSING_INPUT = 1
ERR_MISSING_OUT_DIR = 2
ERR_INVALID_HUE = 3

# used to normalize hues (0-360) to 8-bit values (0-255)
SCALE_HSV_TO_PIL = 255 / 360


##########################
# COMMAND LINE ARGUMENTS #
##########################


parser = argparse.ArgumentParser(
    prog="ColorSwap",
    description="A utility to swap one hue with another (within a threshold) in an image or folder of images.",
    epilog="Brought to you by the EB Online Team",
)

parser.add_argument("-v", action="version", version=f"%(prog)s {VERSION}")

parser.add_argument("input", help="input filepath (image or folder)")
parser.add_argument("output", help="output filepath (must exist if folder)")

parser.add_argument("old_hue", type=int, help="original hue to be swapped")
parser.add_argument("new_hue", type=int, help="hue to replace the original")

parser.add_argument(
    "-t",
    metavar="THRESHOLD",
    type=int,
    default=20,
    help="threshold hue difference (default: 20)",
)

parser.add_argument(
    "-f", metavar="FILTER", default="*.tga*", help="search filter (default: *.tga*)"
)

args = parser.parse_args()

input = args.input
output = args.output
old_hue = args.old_hue
new_hue = args.new_hue
threshold = int(args.t * SCALE_HSV_TO_PIL)
filter = args.f

if old_hue < 0 or old_hue > 360 or new_hue < 0 or new_hue > 360:
    print("Hue values must be between 0 and 360.")
    exit(ERR_INVALID_HUE)

old_hue = int(args.old_hue * SCALE_HSV_TO_PIL)
new_hue = int(args.new_hue * SCALE_HSV_TO_PIL)

if not os.path.exists(args.input):
    print("Input is not an existing file or folder.")
    exit(ERR_MISSING_INPUT)

if os.path.isdir(input) and not os.path.isdir(output):
    print("Output is not an existing folder.")
    exit(ERR_MISSING_OUT_DIR)


####################
# HELPER FUNCTIONS #
####################


def hue_delta(h1: int, h2: int) -> int:
    """Return the distance between two 8-bit hue values."""
    return min(abs(h2 - h1), 255 - abs(h2 - h1))


def add_hues(h1: int, h2: int) -> int:
    """Return the sum of two hues (8-bit)."""
    return (h1 + h2) % 256


def swap_hues(in_path: str, out_path: str, old_hue: int, new_hue: int, threshold: int):
    """Swap hues that fall within threshold and save to output.
    The swap maintains relative differences in hue."""

    im = Image.open(in_path)
    alpha = im.getchannel("A")
    im = im.convert("HSV")
    pixels = im.getdata()

    for i in range(len(pixels)):
        h, s, v = pixels[i]

        if hue_delta(old_hue, h) <= threshold:
            x = i % im.width
            y = i // im.width
            h = add_hues(new_hue, h - old_hue)
            im.putpixel((x, y), (h, s, v))

    im = im.convert("RGBA")
    im.putalpha(alpha)
    im.save(out_path)


################
# MAIN PROGRAM #
################


def main():
    # begin benchmark
    t0 = time.time_ns()

    # batch process folder
    if os.path.isdir(input):
        path_in = Path(input)
        path_out = Path(output)

        files_in = [f for f in path_in.glob(f"**/{filter}") if os.path.isfile(f)]
        files_out = [path_out.joinpath("/".join(f.parts[1:])) for f in files_in]
        number_of_files = len(files_in)

        with ProcessPoolExecutor() as executor:
            for file, _ in zip(
                files_in,
                executor.map(
                    swap_hues,
                    files_in,
                    files_out,
                    repeat(old_hue, number_of_files),
                    repeat(new_hue, number_of_files),
                    repeat(threshold, number_of_files),
                ),
            ):
                print(f"Processing {file}")

    # process single image
    else:
        swap_hues(input, output, old_hue, new_hue, threshold)
        print(f"Processing {input}")

    # end benchmark
    t1 = time.time_ns()

    # report processing time
    delta_t = round((t1 - t0) / (10**9), 3)
    print(f"\nColorSwap took {delta_t} seconds.")


if __name__ == "__main__":
    main()
