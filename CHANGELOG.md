# Changelog

## [0.1] - 2023-02-07

### Added

- CLI app to swap hues in an image (or folder of images).
- Multiprocessing support that uses all CPU cores on a system.
